package ru.t1.chernysheva.tm.repository.dto;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.chernysheva.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.chernysheva.tm.api.service.IPropertyService;
import ru.t1.chernysheva.tm.api.service.dto.IUserDtoService;
import ru.t1.chernysheva.tm.configuration.ServerConfiguration;
import ru.t1.chernysheva.tm.marker.UnitCategory;
import ru.t1.chernysheva.tm.dto.model.ProjectDTO;
import ru.t1.chernysheva.tm.migration.AbstractSchemeTest;

import javax.persistence.EntityManager;
import java.util.*;

import static ru.t1.chernysheva.tm.constant.ProjectConstant.*;

@Category(UnitCategory.class)
public class ProjectDtoRepositoryTest extends AbstractSchemeTest {

    @NotNull
    private ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private IProjectDtoRepository repository;

    @NotNull
    private List<ProjectDTO> projectList;

    @NotNull
    private final IPropertyService propertyService = context.getBean(IPropertyService.class);

    @NotNull
    private final IUserDtoService userService = context.getBean(IUserDtoService.class);

    @Nullable
    private static String USER_ID_1;

    @Nullable
    private static String USER_ID_2;

    private static long USER_ID_COUNTER;

    @Nullable
    private static EntityManager ENTITY_MANAGER;

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void init() {
        USER_ID_COUNTER++;
        USER_ID_1 = userService.create("proj_rep_usr_1_" + USER_ID_COUNTER, "1").getId();
        USER_ID_2 = userService.create("proj_rep_usr_2_" + USER_ID_COUNTER, "1").getId();
        repository = context.getBean(IProjectDtoRepository.class);
        ENTITY_MANAGER = repository.getEntityManager();
        ENTITY_MANAGER.getTransaction().begin();
        projectList = new ArrayList<>();
        for (int i = 1; i <= INIT_COUNT_PROJECTS; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Project_" + i);
            project.setDescription("Description_" + i);
            project.setUserId(USER_ID_1);
            repository.add(USER_ID_1, project);
            projectList.add(project);
        }
        for (int i = 1; i <= INIT_COUNT_PROJECTS; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Project_" + i);
            project.setDescription("Description_" + i);
            project.setUserId(USER_ID_2);
            repository.add(USER_ID_2, project);
            projectList.add(project);
        }
        ENTITY_MANAGER.getTransaction().commit();
        ENTITY_MANAGER.getTransaction().begin();
    }

    @After
    public void clearAfter() {
        ENTITY_MANAGER.getTransaction().commit();
        ENTITY_MANAGER.getTransaction().begin();
        repository.clear(USER_ID_1);
        repository.clear(USER_ID_2);
        ENTITY_MANAGER.getTransaction().commit();
        userService.clear();
    }

    @AfterClass
    public static void closeConnection() {
        ENTITY_MANAGER.close();
    }

    @Test
    public void testAddProjectPositive() {
        ProjectDTO project = new ProjectDTO();
        project.setName("ProjectAddTest");
        project.setDescription("ProjectAddTest desc");
        repository.add(USER_ID_1, project);
    }

    @Test
    public void testClear() {
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_1));
        repository.clear(USER_ID_1);
        Assert.assertEquals(0, repository.getSize(USER_ID_1));

        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_2));
        repository.clear(USER_ID_2);
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test
    public void testFindById() {
        Assert.assertNull(repository.findOneById(USER_ID_1, UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(USER_ID_2, UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        for (@NotNull final ProjectDTO project : projectList) {
            final ProjectDTO foundProject = repository.findOneById(project.getUserId(), project.getId());
            Assert.assertNotNull(foundProject);
            Assert.assertEquals(project.getId(), foundProject.getId());
        }
    }

    @Test
    public void testExistsById() {
        Assert.assertFalse(repository.existsById(USER_ID_1, UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(USER_ID_2, UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        for (@NotNull final ProjectDTO project : projectList) {
            Assert.assertTrue(repository.existsById(project.getUserId(), project.getId()));
        }
    }

    @Test
    public void testFindByIndex() {
        Assert.assertNull(repository.findOneByIndex(USER_ID_1, 9999));
        Assert.assertNull(repository.findOneByIndex(USER_ID_2, 9999));
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            final ProjectDTO foundProject = repository.findOneByIndex(USER_ID_1, i + 1);
            Assert.assertNotNull(foundProject);
            Assert.assertEquals(projectList.get(i).getId(), foundProject.getId());
        }
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            final ProjectDTO foundProject = repository.findOneByIndex(USER_ID_2, i + 1);
            Assert.assertNotNull(foundProject);
            Assert.assertEquals(projectList.get(i + 5).getId(), foundProject.getId());
        }
    }

    @Test
    public void testFindAll() {
        @NotNull List<ProjectDTO> projects = repository.findAll(USER_ID_1);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            Assert.assertEquals(projects.get(i).getId(), projectList.get(i).getId());
        }
        projects = repository.findAll(USER_ID_2);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (int i = 5; i < INIT_COUNT_PROJECTS * 2; i++) {
            Assert.assertEquals(projects.get(i - 5).getId(), projectList.get(i).getId());
        }
    }

    @Test
    public void testFindAllOrderCreated() {
        List<ProjectDTO> projects = repository.findAll(USER_ID_1, CREATED_SORT);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (final ProjectDTO project : projectList) {
            if (project.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getId().equals(m.getId()))
                                .filter(m -> project.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
        projects = repository.findAll(USER_ID_2, CREATED_SORT);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (final ProjectDTO project : projectList) {
            if (project.getUserId().equals(USER_ID_2)) {
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getId().equals(m.getId()))
                                .filter(m -> project.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
    }

    @Test
    public void testFindAllOrderStatus() {
        List<ProjectDTO> projects = repository.findAll(USER_ID_1, STATUS_SORT);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (final ProjectDTO project : projectList) {
            if (project.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getId().equals(m.getId()))
                                .filter(m -> project.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
        projects = repository.findAll(USER_ID_2, STATUS_SORT);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (final ProjectDTO project : projectList) {
            if (project.getUserId().equals(USER_ID_2)) {
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getId().equals(m.getId()))
                                .filter(m -> project.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
    }

    @Test
    public void testFindAllOrderName() {
        List<ProjectDTO> projects = repository.findAll(USER_ID_1, NAME_SORT);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (final ProjectDTO project : projectList) {
            if (project.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getId().equals(m.getId()))
                                .filter(m -> project.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
        projects = repository.findAll(USER_ID_2, NAME_SORT);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (final ProjectDTO project : projectList) {
            if (project.getUserId().equals(USER_ID_2)) {
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getId().equals(m.getId()))
                                .filter(m -> project.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
    }

    @Test
    public void testRemoveById() {
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_1));
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            repository.removeById(USER_ID_1, projectList.get(i).getId());
            Assert.assertNull(repository.findOneById(USER_ID_1, projectList.get(i).getId()));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_1));
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_2));
        for (int i = 5; i < INIT_COUNT_PROJECTS * 2; i++) {
            repository.removeById(USER_ID_2, projectList.get(i).getId());
            Assert.assertNull(repository.findOneById(USER_ID_2, projectList.get(i).getId()));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test
    public void testRemove() {
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_1));
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            repository.remove(USER_ID_1, projectList.get(i));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_1));
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_2));
        for (int i = 5; i < INIT_COUNT_PROJECTS * 2; i++) {
            repository.remove(USER_ID_2, projectList.get(i));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

}
