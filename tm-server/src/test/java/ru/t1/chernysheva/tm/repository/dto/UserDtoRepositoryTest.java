package ru.t1.chernysheva.tm.repository.dto;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.chernysheva.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.chernysheva.tm.configuration.ServerConfiguration;
import ru.t1.chernysheva.tm.marker.UnitCategory;
import ru.t1.chernysheva.tm.dto.model.UserDTO;
import ru.t1.chernysheva.tm.migration.AbstractSchemeTest;

import javax.persistence.EntityManager;
import java.util.*;

import static ru.t1.chernysheva.tm.constant.UserConstant.*;

@Category(UnitCategory.class)
public class UserDtoRepositoryTest extends AbstractSchemeTest {

    @NotNull
    private ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private IUserDtoRepository repository;

    @NotNull
    private List<UserDTO> userList;

    @Nullable
    private static EntityManager ENTITY_MANAGER;

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void init() {
        repository = context.getBean(IUserDtoRepository.class);
        userList = new ArrayList<>();
        ENTITY_MANAGER = repository.getEntityManager();
        ENTITY_MANAGER.getTransaction().begin();
        repository.clear();
        for (int i = 1; i <= INIT_COUNT_USERS; i++) {
            @NotNull final UserDTO user = new UserDTO();
            user.setLogin("User_" + i);
            user.setEmail("User_" + i + "@test.ru");
            user.setFirstName("User_" + i);
            user.setLastName("Userovov_" + i);
            user.setLastName("Userovich_" + i);
            repository.add(user);
            userList.add(user);
        }
        ENTITY_MANAGER.getTransaction().commit();
    }

    @After
    public void ClearAfter() {
        ENTITY_MANAGER.getTransaction().begin();
        repository.clear();
        ENTITY_MANAGER.getTransaction().commit();
    }

    @AfterClass
    public static void closeConnection() {
        ENTITY_MANAGER.close();
    }

    @Test
    public void testAddUserPositive() {
        UserDTO user = new UserDTO();
        user.setLogin("UserAddTest");
        ENTITY_MANAGER.getTransaction().begin();
        repository.add(user);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertEquals(INIT_COUNT_USERS + 1, repository.getSize());
    }

    @Test
    public void testClear() {
        Assert.assertEquals(INIT_COUNT_USERS, repository.getSize());
        ENTITY_MANAGER.getTransaction().begin();
        repository.clear();
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testFindById() {
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString()));
        for (@NotNull final UserDTO user : userList) {
            final UserDTO foundUser = repository.findOneById(user.getId());
            Assert.assertNotNull(foundUser);
            Assert.assertEquals(user.getId(), foundUser.getId());
        }
    }

    @Test
    public void testExistsById() {
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString()));
        for (@NotNull final UserDTO user : userList) {
            Assert.assertTrue(repository.existsById(user.getId()));
        }
    }

    @Test
    public void testFindByIndex() {
        Assert.assertNull(repository.findOneByIndex(9999));
        for (final UserDTO user : userList) {
            final UserDTO foundUser = repository.findOneByIndex(userList.indexOf(user) + 1);
            Assert.assertNotNull(foundUser);
            Assert.assertNotNull(
                    userList.stream()
                            .filter(m -> user.getId().equals(m.getId()))
                            .findFirst()
                            .orElse(null)
            );
        }
    }

    @Test
    public void testFindAll() {
        List<UserDTO> users = repository.findAll();
        Assert.assertNotNull(users);
        Assert.assertEquals(userList.size(), users.size());
        for (final UserDTO user : userList) {
            Assert.assertNotNull(
                    userList.stream()
                            .filter(m -> user.getId().equals(m.getId()))
                            .findFirst()
                            .orElse(null)
            );
        }
    }

    @Test
    public void testRemoveByIdPositive() {
        Assert.assertEquals(INIT_COUNT_USERS, repository.getSize());
        for (final UserDTO user : userList) {
            ENTITY_MANAGER.getTransaction().begin();
            repository.removeById(user.getId());
            ENTITY_MANAGER.getTransaction().commit();
            Assert.assertNull(repository.findOneById(user.getId()));
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testRemovePositive() {
        Assert.assertEquals(INIT_COUNT_USERS, repository.getSize());
        for (final UserDTO user : userList) {
            ENTITY_MANAGER.getTransaction().begin();
            repository.remove(user);
            ENTITY_MANAGER.getTransaction().commit();
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testExistsLogin() {
        for (final UserDTO user : userList) {
            Assert.assertTrue(repository.isLoginExist(user.getLogin()));
        }
    }

    @Test
    public void testFindByLogin() {
        for (final UserDTO user : userList) {
            Assert.assertNotNull(repository.findByLogin(user.getLogin()));
        }
    }

    @Test
    public void testExistsEmail() {
        for (final UserDTO user : userList) {
            Assert.assertTrue(repository.isEmailExist(user.getEmail()));
        }
    }

    @Test
    public void testFindByEmail() {
        for (final UserDTO user : userList) {
            Assert.assertNotNull(repository.findByEmail(user.getEmail()));
        }
    }

}
