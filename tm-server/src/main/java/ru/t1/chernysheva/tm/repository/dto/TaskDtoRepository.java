package ru.t1.chernysheva.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.chernysheva.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.chernysheva.tm.dto.model.TaskDTO;

import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDTO> implements ITaskDtoRepository {

    @Override
    protected Class<TaskDTO> getEntityClass() {
        return TaskDTO.class;
    }

    @Override
    @NotNull
    public List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.userId = :userId AND m.projectId = :projectId";
        return entityManager.createQuery(query, getEntityClass()).setParameter("userId", userId).setParameter("projectId", projectId).getResultList();
    }

}
