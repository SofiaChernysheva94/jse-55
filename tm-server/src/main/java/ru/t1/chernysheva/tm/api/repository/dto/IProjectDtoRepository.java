package ru.t1.chernysheva.tm.api.repository.dto;

import ru.t1.chernysheva.tm.dto.model.ProjectDTO;

public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDTO> {
}
