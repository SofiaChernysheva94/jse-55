package ru.t1.chernysheva.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.chernysheva.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.chernysheva.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.chernysheva.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.chernysheva.tm.dto.model.ProjectDTO;
import ru.t1.chernysheva.tm.dto.model.TaskDTO;
import ru.t1.chernysheva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.chernysheva.tm.exception.entity.TaskNotFoundException;
import ru.t1.chernysheva.tm.exception.field.IndexIncorrectException;
import ru.t1.chernysheva.tm.exception.field.ProjectIdEmptyException;
import ru.t1.chernysheva.tm.exception.field.TaskIdEmptyException;
import ru.t1.chernysheva.tm.exception.field.UserIdEmptyException;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectTaskDtoService implements IProjectTaskDtoService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    private IProjectDtoRepository getProjectDtoRepository() {
        return context.getBean(IProjectDtoRepository.class);
    }

    @NotNull
    private ITaskDtoRepository getTaskDtoRepository() {
        return context.getBean(ITaskDtoRepository.class);
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final IProjectDtoRepository projectRepository = getProjectDtoRepository();
        @NotNull final ITaskDtoRepository taskRepository = getTaskDtoRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        @NotNull final EntityManager entityManagerProject = projectRepository.getEntityManager();
        @Nullable final TaskDTO task;
        try {
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(projectId);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
            entityManagerProject.close();
        }
    }

    @Override
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final IProjectDtoRepository projectRepository = getProjectDtoRepository();
        @NotNull final ITaskDtoRepository taskRepository = getTaskDtoRepository();
        @NotNull final EntityManager entityManagerTask = taskRepository.getEntityManager();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        @NotNull final List<TaskDTO> tasks;
        try {
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            tasks = taskRepository.findAllByProjectId(userId, projectId);
            entityManagerTask.getTransaction().begin();
            for (final TaskDTO task : tasks) taskRepository.removeById(userId, task.getId());
            entityManagerTask.getTransaction().commit();
            entityManager.getTransaction().begin();
            projectRepository.removeById(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
            entityManagerTask.close();
        }
    }

    @Override
    public void removeProjectByIndex(@Nullable final String userId, @Nullable final Integer projectIndex) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectIndex == null || projectIndex < 1) throw new IndexIncorrectException();
        @NotNull final IProjectDtoRepository projectRepository = getProjectDtoRepository();
        @NotNull final ITaskDtoRepository taskRepository = getTaskDtoRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        @NotNull final EntityManager entityManagerTask = taskRepository.getEntityManager();
        @NotNull final List<TaskDTO> tasks;
        @Nullable ProjectDTO project;
        try {
            project = projectRepository.findOneByIndex(userId, projectIndex);
            if (project == null) throw new ProjectNotFoundException();
            tasks = taskRepository.findAllByProjectId(userId, project.getId());
            entityManagerTask.getTransaction().begin();
            for (final TaskDTO task : tasks) taskRepository.removeById(userId, task.getId());
            entityManagerTask.getTransaction().commit();
            entityManager.getTransaction().begin();
            projectRepository.removeById(userId, project.getId());
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
            entityManagerTask.close();
        }
    }

    @Override
    public void unbindTaskFromProject(@Nullable final String userId,
                                      @Nullable final String projectId,
                                      @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final IProjectDtoRepository projectRepository = getProjectDtoRepository();
        @NotNull final ITaskDtoRepository taskRepository = getTaskDtoRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        @NotNull final EntityManager entityManagerProject = projectRepository.getEntityManager();
        @Nullable final TaskDTO task;
        try {
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(null);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
            entityManagerProject.close();
        }
    }

}
