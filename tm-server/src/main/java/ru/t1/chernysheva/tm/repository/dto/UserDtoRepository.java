package ru.t1.chernysheva.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.chernysheva.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.chernysheva.tm.dto.model.UserDTO;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class UserDtoRepository extends AbstractDtoRepository<UserDTO> implements IUserDtoRepository {

    @Override
    protected Class<UserDTO> getEntityClass() {
        return UserDTO.class;
    }

    @Override
    @Nullable
    public UserDTO findByLogin(@NotNull String login) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.login = :login";
        return entityManager.createQuery(query, getEntityClass()).setParameter("login", login).getResultStream().findFirst().orElse(null);
    }

    @Override
    @Nullable
    public UserDTO findByEmail(@NotNull String email) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.email = :email";
        return entityManager.createQuery(query, getEntityClass()).setParameter("email", email).getResultStream().findFirst().orElse(null);
    }

    @Override
    @NotNull
    public Boolean isLoginExist(@NotNull String login) {
        return findByLogin(login) != null;
    }

    @Override
    @NotNull
    public Boolean isEmailExist(@NotNull String email) {
        return findByEmail(email) != null;
    }

}
