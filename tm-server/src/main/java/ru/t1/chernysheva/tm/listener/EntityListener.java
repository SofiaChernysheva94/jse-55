package ru.t1.chernysheva.tm.listener;

import lombok.NoArgsConstructor;
import org.hibernate.event.spi.*;
import org.hibernate.persister.entity.EntityPersister;
import ru.t1.chernysheva.tm.log.OperationEvent;
import ru.t1.chernysheva.tm.log.OperationType;

@NoArgsConstructor
public final class EntityListener implements PostInsertEventListener, PostDeleteEventListener, PostUpdateEventListener {

    private JmsLoggerProducer jmsLoggerProducer;

    public EntityListener(final JmsLoggerProducer jmsLoggerProducer) {
        this.jmsLoggerProducer = jmsLoggerProducer;
    }

    @Override
    public void onPostDelete(final PostDeleteEvent event) {
        log(OperationType.DELETE, event.getEntity());
    }

    @Override
    public void onPostInsert(final PostInsertEvent event) {
        log(OperationType.INSERT, event.getEntity());
    }

    @Override
    public void onPostUpdate(final PostUpdateEvent event) {
        log(OperationType.UPDATE, event.getEntity());
    }

    @Override
    public boolean requiresPostCommitHanding(final EntityPersister entityPersister) {
        return false;
    }

    private void log(final OperationType type, final Object entity) {
        jmsLoggerProducer.send(new OperationEvent(type, entity));
    }

}
