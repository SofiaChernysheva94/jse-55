package ru.t1.chernysheva.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.enumerated.Sort;
import ru.t1.chernysheva.tm.model.AbstractUserOwnedModel;

import javax.persistence.EntityManager;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> {

    @NotNull EntityManager getEntityManager();

    void add(@NotNull String userId, @NotNull M model);

    void clear(@NotNull String userId);

    boolean existsById(@NotNull String userId, @NotNull String id);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(@NotNull String userId, @Nullable final Sort sort);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull String userId, @NotNull Integer index);

    int getSize(@NotNull String userId);

    void remove(@NotNull String userId, @NotNull M model);

    void removeById(@NotNull String userId, @NotNull String id);

    void update(@NotNull M model);

}
