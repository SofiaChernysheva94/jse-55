package ru.t1.chernysheva.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.chernysheva.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.chernysheva.tm.api.service.dto.ITaskDtoService;
import ru.t1.chernysheva.tm.dto.model.TaskDTO;
import ru.t1.chernysheva.tm.enumerated.Sort;
import ru.t1.chernysheva.tm.enumerated.Status;
import ru.t1.chernysheva.tm.exception.entity.TaskNotFoundException;
import ru.t1.chernysheva.tm.exception.field.*;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class TaskDtoService implements ITaskDtoService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    private ITaskDtoRepository getRepository() {
        return context.getBean(ITaskDtoRepository.class);
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ITaskDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ITaskDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull List<TaskDTO> result;
        try {
            result = repository.findAll(userId);
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final ITaskDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        boolean result;
        try {
            result = repository.existsById(userId, id);
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final ITaskDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable TaskDTO result;
        try {
            result = repository.findOneById(userId, id);
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @NotNull final ITaskDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable TaskDTO result;
        try {
            result = repository.findOneByIndex(userId, index);
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final ITaskDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @NotNull final ITaskDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable TaskDTO task;
        try {
            task = repository.findOneByIndex(userId, index);
            if (task == null) throw new TaskNotFoundException();
            entityManager.getTransaction().begin();
            repository.remove(userId, task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    @SuppressWarnings({"unchecked"})
    public List<TaskDTO> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ITaskDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull List<TaskDTO> result;
        try {
            result = repository.findAll(userId, sort);
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    public void changeTaskStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final ITaskDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable final TaskDTO task;
        try {
            task = repository.findOneById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            if (status == null) throw new StatusIncorrectException();
            task.setStatus(status);
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void changeTaskStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @NotNull final ITaskDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable final TaskDTO task;
        try {
            task = repository.findOneByIndex(userId, index);
            if (task == null) throw new TaskNotFoundException();
            if (status == null) throw new StatusIncorrectException();
            task.setStatus(status);
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final ITaskDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable final List<TaskDTO> result;
        try {
            result = repository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final ITaskDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable TaskDTO task = new TaskDTO();
        try {
            task.setName(name);
            if (description != null && !description.isEmpty())
                task.setDescription(description);
            entityManager.getTransaction().begin();
            repository.add(userId, task);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void add(@Nullable final String userId, @Nullable final TaskDTO task) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new TaskNotFoundException();
        @NotNull final ITaskDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(userId, task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final ITaskDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable TaskDTO task;
        try {
            task = repository.findOneById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            task.setName(name);
            if (description != null && !description.isEmpty())
                task.setDescription(description);
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final ITaskDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable TaskDTO task;
        try {
            task = repository.findOneByIndex(userId, index);
            if (task == null) throw new TaskNotFoundException();
            task.setName(name);
            if (description != null && !description.isEmpty())
                task.setDescription(description);
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
