package ru.t1.chernysheva.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.chernysheva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.chernysheva.tm.api.service.IAuthService;
import ru.t1.chernysheva.tm.api.service.dto.IUserDtoService;
import ru.t1.chernysheva.tm.dto.model.SessionDTO;
import ru.t1.chernysheva.tm.dto.model.UserDTO;
import ru.t1.chernysheva.tm.dto.request.UserLoginRequest;
import ru.t1.chernysheva.tm.dto.request.UserLogoutRequest;
import ru.t1.chernysheva.tm.dto.request.UserProfileRequest;
import ru.t1.chernysheva.tm.dto.response.UserLoginResponse;
import ru.t1.chernysheva.tm.dto.response.UserLogoutResponse;
import ru.t1.chernysheva.tm.dto.response.UserProfileResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@AllArgsConstructor
@WebService(endpointInterface = "ru.t1.chernysheva.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    @NotNull
    @Autowired
    private IAuthService authService;

    @NotNull
    @Autowired
    private IUserDtoService userDtoService;

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLoginRequest request
    ) throws Exception {
        @NotNull final String token = authService.login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLogoutRequest request
    ) throws Exception {
        final SessionDTO session = check(request);
        authService.invalidate(session);
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserProfileRequest request
    ) throws Exception {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final UserDTO user = userDtoService.findById(userId);
        return new UserProfileResponse(user);
    }

}
