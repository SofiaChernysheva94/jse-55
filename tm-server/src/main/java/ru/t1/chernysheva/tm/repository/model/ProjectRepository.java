package ru.t1.chernysheva.tm.repository.model;

import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.chernysheva.tm.api.repository.model.IProjectRepository;
import ru.t1.chernysheva.tm.model.Project;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Override
    protected Class<Project> getEntityClass() {
        return Project.class;
    }

}
