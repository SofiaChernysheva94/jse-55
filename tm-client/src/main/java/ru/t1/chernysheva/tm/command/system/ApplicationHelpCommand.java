package ru.t1.chernysheva.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.chernysheva.tm.api.model.ICommand;
import ru.t1.chernysheva.tm.command.AbstractCommand;

import java.util.Collection;

@Component
public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-h";

    @NotNull
    public static final String DESCRIPTION = "Display list of terminal command.";

    @NotNull
    public static final String NAME = "help";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = commandService.getTerminalCommands();
        for (@NotNull final ICommand command : commands) System.out.println(command);
    }

}
