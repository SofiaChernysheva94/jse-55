package ru.t1.chernysheva.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.chernysheva.tm.dto.request.UserChangePasswordRequest;
import ru.t1.chernysheva.tm.util.TerminalUtil;

@Component
public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Change password for current user.";

    @NotNull
    public static final String NAME = "user-change-password";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();

        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(getToken());
        request.setPassword(password);

        userEndpoint.changeUserPassword(request);
    }

}
