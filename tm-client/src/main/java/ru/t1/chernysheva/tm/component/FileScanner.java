package ru.t1.chernysheva.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.chernysheva.tm.api.service.ICommandService;
import ru.t1.chernysheva.tm.api.service.ILoggerService;
import ru.t1.chernysheva.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public final class FileScanner {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Autowired
    private ICommandService commandService;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final File folder = new File("./");

    public void start() {
        @NotNull final Iterable<AbstractCommand> commands = commandService.getCommandsWithArgument();
        commands.forEach(e -> this.commands.add(e.getName()));
        es.scheduleWithFixedDelay(this::process, 0, 1, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    private void process() {
        for (@NotNull final File file : folder.listFiles()) {
            if (file.isDirectory()) continue;
            @NotNull final String fileName = file.getName();
            final boolean check = commands.contains(fileName);
            if (check) {
                try {
                    file.delete();
                    bootstrap.processCommand(fileName);
                } catch (@NotNull final Exception e) {
                    loggerService.error(e);
                }
            }
        }
    }

}
