package ru.t1.chernysheva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.dto.request.ProjectStartByIdRequest;
import ru.t1.chernysheva.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Start project by id.";

    @NotNull
    public static final String NAME = "project-start-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(getToken());
        request.setId(id);

        projectEndpoint.startProjectById(request);
    }

}
