package ru.t1.chernysheva.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.hibernate.annotations.Cache;
import ru.t1.chernysheva.tm.api.model.IWBS;
import ru.t1.chernysheva.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_project")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class ProjectDTO extends AbstractUserOwnedModelDTO implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(nullable = false, name = "name")
    private String name = "";

    @NotNull
    @Column(name = "description")
    private String description = "";

    @NotNull
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(name = "created")
    private Date created = new Date();

    @NotNull
    @Override
    public String toString() {
        return this.getName() + " | Status: " + status.getDisplayName() + " |";
    }

}
