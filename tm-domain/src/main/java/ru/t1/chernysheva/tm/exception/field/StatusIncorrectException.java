package ru.t1.chernysheva.tm.exception.field;

public final class StatusIncorrectException extends AbstractFieldException {

    public StatusIncorrectException() {
        super("Error! This status does not exist in the system...");
    }

}
