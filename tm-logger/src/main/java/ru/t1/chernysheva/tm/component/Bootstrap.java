package ru.t1.chernysheva.tm.component;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.chernysheva.tm.api.IReceiverService;
import ru.t1.chernysheva.tm.listener.LoggerListener;
import ru.t1.chernysheva.tm.service.PropertyLoggerService;
import ru.t1.chernysheva.tm.service.ReceiverService;

@NoArgsConstructor
@Component
public final class Bootstrap {

    @NotNull
    private PropertyLoggerService propertyLoggerService = new PropertyLoggerService();

    @NotNull
    @Autowired
    private ReceiverService receiverService;

    @NotNull
    @Autowired
    private LoggerListener loggerListener;

    @SneakyThrows
    public void start() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory();
        factory.setBrokerURL(propertyLoggerService.getBrokerURL());
        factory.setConnectResponseTimeout(30000);
        factory.setTrustAllPackages(true);
        @NotNull final IReceiverService receiverService = new ReceiverService(factory);
        receiverService.receive(new LoggerListener());
    }

}
