package ru.t1.chernysheva.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.chernysheva.tm.api.ILoggerService;

import org.bson.Document;
import java.util.LinkedHashMap;
import java.util.Map;


@Service
public final class LoggerService implements ILoggerService {

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private PropertyLoggerService propertyLoggerService = new PropertyLoggerService();

    @NotNull
    private final MongoClient mongoClient = new MongoClient(getDBHost(), getDBPort());

    @NotNull
    private final MongoDatabase mongoDatabase = mongoClient.getDatabase(getDBName());

    @Override
    @SneakyThrows
    public void writeLog(@NotNull final String message) {
        @NotNull final Map<String, Object> event = objectMapper.readValue(message, LinkedHashMap.class);
        @NotNull final String table = event.get("table").toString();

        if (mongoDatabase.getCollection(table) == null) mongoDatabase.createCollection(table);
        @NotNull final MongoCollection<Document> collection = mongoDatabase.getCollection(table);
        collection.insertOne(new Document(event));
    }

    private String getDBHost() {
        return propertyLoggerService.getJMSHost();
    }

    private int getDBPort() {
        return Integer.parseInt(propertyLoggerService.getJMSPort());
    }

    private String getDBName() {
        return propertyLoggerService.getJMSDBName();
    }

}
