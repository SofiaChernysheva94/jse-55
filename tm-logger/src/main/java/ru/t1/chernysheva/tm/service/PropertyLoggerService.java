package ru.t1.chernysheva.tm.service;

import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.api.IPropertyLoggerService;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.ClassLoader.getSystemResourceAsStream;

public final class PropertyLoggerService implements IPropertyLoggerService {
    @NotNull
    public static final String APPLICATION_FILE_NAME_KEY = "application.config";

    @NotNull
    public static final String APPLICATION_FILE_NAME_DEFAULT = "application.properties";

    @NotNull
    private static final String JMS_PORT_DEFAULT = "61616";

    @NotNull
    private static final String JMS_PORT_KEY = "jms.port";

    @NotNull
    private static final String JMS_HOST_DEFAULT = "localhost";

    @NotNull
    private static final String JMS_HOST_KEY = "jms.host";

    @NotNull
    private static final String JMS_DATABASE_DEFAULT = "tm-log";

    @NotNull
    private static final String JMS_DATABASE_KEY = "jms.database";

    @NotNull
    private static final String JMS_BROKER_URL_DEFAULT = "tcp://tm-server-alpha:61616";

    @NotNull
    private static final String JMS_BROKER_URL_KEY = "jms.url";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyLoggerService() {
        final boolean existsConfig = isExistsExternalConfig();
        if (existsConfig) loadExternalConfig(properties);
        else loadInternalConfig(properties);
    }

    @SneakyThrows
    private void loadInternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = APPLICATION_FILE_NAME_DEFAULT;
        @Cleanup @Nullable final InputStream inputStream = getSystemResourceAsStream(name);
        if (inputStream == null) return;
        properties.load(inputStream);
    }

    @SneakyThrows
    private void loadExternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        @Cleanup @NotNull final InputStream inputStream = new FileInputStream(file);
        properties.load(inputStream);
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".","_");
    }

    @NotNull
    @Override
    public String getApplicationConfig() {
        return getStringValue(APPLICATION_FILE_NAME_KEY, APPLICATION_FILE_NAME_DEFAULT);
    }

    private boolean isExistsExternalConfig() {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        return file.exists();
    }

    @Override
    public @NotNull String getJMSHost() {
        return getStringValue(JMS_HOST_KEY, JMS_HOST_DEFAULT);
    }

    @Override
    public @NotNull String getJMSPort() {
        return getStringValue(JMS_PORT_KEY, JMS_PORT_DEFAULT);
    }

    @Override
    public @NotNull String getJMSDBName() {
        return getStringValue(JMS_DATABASE_KEY, JMS_DATABASE_DEFAULT);
    }

    @Override
    public @NotNull String getBrokerURL() {
        return getStringValue(JMS_BROKER_URL_KEY, JMS_BROKER_URL_DEFAULT);
    }

}
