package ru.t1.chernysheva.tm.api;

import org.jetbrains.annotations.NotNull;

public interface IPropertyLoggerService {

    @NotNull
    String getApplicationConfig();

    @NotNull
    String getJMSHost();

    @NotNull
    String getJMSPort();

    @NotNull
    String getJMSDBName();

    @NotNull
    String getBrokerURL();

}
